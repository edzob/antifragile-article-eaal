\section{Introduction}

The goal of organizations, operating in a socio-economic context, is to remain significant for its stakeholders.
Stakeholders are owners, employees and consumers \cite{book-optland-2008}.
In order to do so, the organization has to stay aligned with its environment.
As such, there is a constant `dance' in which the organization adapts to changing conditions in the market place, and at the same time also influences this environment - all in the name of avoiding \Term{strategic drift} (see e.g. \cite{2006-Wit-Strategy}). 

It is often said that the rate at which organizations have to change increases rapidly and that therefore we live in uncertain times \cite{book-hutchins-2018} characterized by high volatility, uncertainty, complexity, and ambiguity (\Term{VUCA}) \cite{article-bennet-2014-bh, article-bennet-2014-bhr,book-mack-2015, book-hutchins-2018}. 
For example, Anthony et al. state that \enquote{We're entering a period of heightened volatility for leading companies across a range of industries, with the next ten years shaping up to be the most potentially turbulent in modern history} \cite{article-anthony-2016}.
We use the term \Term{stressor} to denote an event in the environment of the organization that increases the need to adapt/ re-align in organizations. 
Typical stressors that organizations are facing are the COVID-19 pandemic, ever changing customer demands, and the effects of digital transformation (e.g. \cite{2013-Fitzgerald-digitaltransformation, artile-aghina-2017, 2019-Kane-TechnologyFallacy}). 

In order to stay relevant/ stay aligned with its environment, it was long thought that organizations should strive to be \Term{robust} with respect to external stressors. A more recent insight is that \Term{robustness} is the middle ground between \Term{fragility} (the organization deteriorates as a result of stressors) and \Term{antifragility} (the organization improves as a result of stressors) \cite{article-antifragile-taleb-2013, book-taleb-2012}. Therefore, a more ambitious goal is to become antifragile.

 This brings us to the articulation of the objective for organizations that we hope to tackle in this paper: organizations have to find a way to stay relevant in the current VUCA world and be able to survive disruptive black swan stressors events. 
 Our hypothesis is that this can be achieved if the organization has \emph{antifragile} properties. 
 This hypothesis is supported by literature from the field of risk management, complexity theory/ complex adaptive systems as well as enterprise governance (e.g. \cite{book-hutchins-2018, article-martin-breen-2011, thesis-kastner-2017, thesis-henriksson-2016, artile-aghina-2017, book-hoogervorst-2017,book-taleb-2012}).  

This leads to the following research question: \emph{What attributes make an organization antifragile?} 

The main contribution of this study is that we provide insight in the characteristics of resilient and antifragile organizations. 
For scientists, our framework extends theories of organizational design. 
For practitioners, the framework provides attributes which are useful in designing organizations or parts of it.

To answer the research question, we will first frame our research in more detail by clarifying key terms and our way of looking at organizations (Section~\ref{sec:framing}). 
In Section~\ref{sec:methodology}, we then present our research methodology which roughly follows the lines of \emph{design science} and relies on literature reviews, and interviews with experts and practitioners as input for the design of our framework. 
In the remainder of the article, we present our results (Section~\ref{sec:results}) and conclusions (Section~\ref{sec:conclusion}), including a critical discussion of the results and future research. 

The main result of our research is the EAAL Framework,  
which is validated by experts and practitioners.
The EAAL framework is a synthesis of the attributes found in the selected literature,
and is constructed from the concepts found in that same literature.