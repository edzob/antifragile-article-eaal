# antifragile-article-eaal

Goal is to write an (academic) article for the [CBI 2021](https://portal.cbi-series.org/).  

The content will be a summary (inspired by) the thesis "Defining Antifragility and the application
on Organisation Design - a literature study in the field of antifragility, applied in the context of organisation design" ([DOI](https://doi.org/10.5281/zenodo.3719388), [Gitlab Git](https://gitlab.com/edzob/antifragile-research), [Gitlab Wiki](https://gitlab.com/edzob/antifragile-research/-/wikis/home)) 

# Copyright
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

Creative Commons, Attribution-ShareAlike 4.0 International

*You are free to:*    
Share — copy and redistribute the material in any medium or format  
Adapt — remix, transform, and build upon the material for any purpose, even commercially.

*Under the following terms:*     
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.   
ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.    
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

# Research Infrastructure
The Research Infrastructure is inspired by the [Research Infrastructure](https://gitlab.com/edzob/antifragile-research/-/wikis/Research-Infrastructure). 

## Tooling and Conventions

1. Language ?
    1. EN-US
1. Formatting style
    1. after each '.' start an newline (return)
    1. Before each section, subsection, subsubsection an empty line
    1. Sentences are max. 2.5 lines in the end product.
1. Reference ?
    1. Superscript IEEE, Footnote with APA or APA
1. Layout Language
    1. Latex 
1. Literature Reference Database
    1. BibTEX
1. Literature Reference processor
    1. ``\usepackage[numbers]{natbib}`` ``\bibliographystyle{IEEEtranN}`` from [Imperial College London - Library](https://www.imperial.ac.uk/media/imperial-college/administration-and-support-services/library/public/LaTeX-example-IEEE-apr-2019.pdf)
1. Textwriter
    1. Overleaf
    1. Sublime Text v3
    1. AWS Cloud9
1. Latex Compiler / PDF Generation
    1. Overleaf
1. Code sharing
    1. Gitlab - Repository

# Optional Tooling
1. Website
    1. Hostname ?
    1. [Gitlab - Pages](https://docs.gitlab.com/ee/user/project/pages/)
    1. [Gitlab - CI/CD](https://about.gitlab.com/topics/ci-cd/) 
    1. [HUGO (SSG & Markdown)](https://gitlab.com/pages/hugo)
1. Issua Tracker
    1. [Gitlab - Issuetracker / kanban](https://docs.gitlab.com/ee/user/project/issues/)
1. References - Bibtex management
    1. [Zotero](https://www.zotero.org/) - FAT OSS Client (no experience)
    1. [Zotero bib](https://zbib.org/) - lightweight webversion of Zotero
