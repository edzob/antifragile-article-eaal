\section{Results}
\label{sec:results}

\subsection{EAAL framework}


The EAAL framework is the result of the literature study and comprises attributes that require consideration in the design of resilient and antifragile organizations. Figure \ref{fig:eaal-framework} contains the EAAL framework. Table \ref{tab:eaal-framework} defines the different attributes of the EAAL framework. 

\begin{figure}[!tbh]
    \centering
    \fbox{\includegraphics[width=\linewidth]{eaal-framework}}
    \caption{EAAL Framework}
    \label{fig:eaal-framework}
\end{figure}

\begin{table*}[!tbh]
    \caption{EAAL Attributes}
    \label{tab:eaal-framework}
    \centering
    
    \begin{tabularx}{\textwidth}{l|X|R{2.5cm}}

        \textbf{Attribute} &
        \textbf{Description} &
        \textbf{References}
        \\
        \hline 
        
        %\multicolumn{3}{l}{\textbf{Engineering Resilience} }  \\\hline
        
        Top-down C\&C & 
        Top-down command and control applies when  
        an employee does not have the freedom 
        to decide their own action
        but has to follow instructions 
        from the organizational hierarchy. 

        The careful design of the features of an iPhone or a good pen 
        are examples of limited freedom of movement in the product itself. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas}, 
        \cite{thesis-henriksson-2016}, 
        \cite{thesis-kastner-2017}, 
        \cite{article-oreilly-2019}
        \\\hline

        Micro-Management  & 
        Micro-management entails the freedom in the use of the product. 
        A detailed working instruction describing a business process, 
        results in no freedom for the employee in the execution of their job. 
        An example is a Lego building block. 
        It is engineered and fabricated 
        with the greatest detail resulting in a building block 
        that is almost completely robust. 
        Lego has a very small resilience behavior through engineering. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas}, 
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016}, 
        \cite{article-org-ghasemi-2017}, 
        \cite{thesis-kastner-2017}
        \\
        \hline

        %\addlinespace
        %\multicolumn{3}{l}{\textbf{Systems Resilience} } \\\hline
        
        Redundancy & 
        Redundancy is about having not a single point of failure 
        by making use of duplication. 
        
        An example is a backup electricity generator. 
        Another example is local government 
        as backup system of the central government. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}, 
        \cite{article-org-kennon-2015}, 
        \cite{article-oreilly-2019}
        \\\hline       
        
        Modularity & 
        Modularity is the degree that components may be separated and recombined, 
        often with the benefit of flexibility. 
        For example, a car with a standard chassis onto which different components can be connected creating a unique car. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016}, 
        \cite{article-oreilly-2019}
        \\\hline        
        
        Loosely coupled & 
        Loosely coupled is the degree of dependency on the exact working of another module. It is important to understand that there is always some degree of coupling. Loosely coupled is also known as ‘weak links‘, ‘uncoupling‘, 
        ‘loose/tight coupling‘, or ‘a low level of interconnectedness between components‘. For example, 
        when there are new employees introduced at the finance department 
        this should not impact the taste of the coffee at the same office. &
        \cite{article-martin-breen-2011},
        \cite{article-johnson-2013-cas},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016}, 
        \cite{article-org-ghasemi-2017},
        \cite{article-oreilly-2019},
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{\textbf{CAS Resilience} } \\\hline
      
        Diversity &  
        Diversity is the ability to solve a problem in more than one way with different components. 
        Optionality, the availability of options, is a specialisation of diversity.
        An example is that within a team you want diverse co-workers 
        since other types of people come up with other types of solutions.
        & 
        \cite{article-martin-breen-2011}, 
        \cite{book-taleb-2012},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017},
        \cite{article-oreilly-2019}
        \\\hline    
        
        Non-Monotonicity & 
        Non-Monotonicity is learning from bad experiences. 
        Mistakes and failures can lead to new information. 
        As new information becomes available it defeats previous thinking, 
        which can result in new practices and approaches. & 
        \cite{article-johnson-2013-cas},  
        \cite{article-se-gorgeon-2015},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}
        \\\hline    
        
        Emergence & 
        When there is little or no traceable relation between
        micro and macro level output then emergence is there.
        This is the situation where random things (unintended states) appear 
        more often and X-events (black swans) appear. 
        The law or requisite variety applied in this reasoning, 
        makes that internal emergence counters external emergence, 
        and this subsequently leads to antifragility & 
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},  
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017},
        \cite{article-org-ghasemi-2017}
        \\\hline        
        
        Self-Organization & 
        Self-organization is a process where some form 
        of overall order arises from local interactions 
        between parts of an initially disordered system. For example, 
        students sitting together in the school cafeteria. & 
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017}
        \\\hline    
        
        Insert low-level stress & 
        Continuous Improvement is achieved 
        by inserting low-level of stress continuously into a learning system. 
        This will keep the system sharp all the time. & 
        \cite{book-taleb-2012},
        \cite{article-org-kennon-2015},  
        \cite{article-se-gorgeon-2015},
        \cite{article-org-ghasemi-2017} 
        \\\hline    
        
        Network-connections & 
        A network is created by connections to other nodes. 
        More connections increase the potential for optionality 
        for new constructions and also new functionalities. & 
        \cite{article-johnson-2013-cas}, 
        \cite{article-se-gorgeon-2015},
        \cite{thesis-henriksson-2016},
        \cite{book-hole-2016},
        \cite{thesis-kastner-2017}, 
        \cite{article-org-ghasemi-2017}, 
        \cite{article-markey-2018},
        \cite{article-oreilly-2019} 
        \\\hline 

        Fail Fast & 
        The other combined attributes in this group enable the possibility 
        to execute the strategy “fail fast”. & 
        \cite{article-org-kennon-2015},   
        \cite{article-se-gorgeon-2015},    
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{\textbf{Antifragile} } \\\hline
       
        Resources to invest & 
        Opportunities can only be seized when there are resources free to do so. 
        Resources can be money but also time and labor. 
        To survive, a black swan investment should be possible when required & 
        \cite{book-taleb-2012},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017},
        \cite{thesis-henriksson-2016} 
        \\\hline  

        Seneca’s barbell &  
        To be antifragile a robust sub-system 
        is needed to which 80-90\% predictable value with low risk is situated.
        The remaining 10-20\% should be used for high return 
        on investment activities.  & 
        \cite{book-taleb-2012},
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016}
        \\\hline 
        
        Insert randomness & 
        When insert-low-level stress and fail fast delivers no issues 
        the next step is to insert randomness into the systems. 
        A great example of this is chaos engineering by Netflix 
        or the HackerOne bug-bounty system. & 
        \cite{book-taleb-2012},
        \cite{article-org-kennon-2015},
        \cite{article-se-gorgeon-2015},
        \cite{article-org-ghasemi-2017}
        \\\hline 
        
        Reduce naive intervention & 
        Naive intervention is an intervention based on a model 
        and reductionistic logic which ignores experience. 
        An example is not listening to the experienced 
        but not so articulate employee, 
        or by ignoring the balance nature has found in an ecosystem. & 
        \cite{book-taleb-2012}, 
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017}
        \\\hline

        Skin in the game &  
        Make certain that the person making the decision 
        and doing the work has a pain and gain relation with the outcome. 
        This goes beyond having a feedback system in place. 
        This goes beyond having KPI’s in place. 
        An example is that when working Agile scrum, 
        the product owner should be a co-worker in the team 
        for whom the solution is build. & 
        \cite{book-taleb-2012}, 
        \cite{thesis-henriksson-2016},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017}
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{ \textbf{Learning Organization} }\\\hline
        
        Personal mastery & 
        Personal mastery is a discipline of continually clarifying and deepening our personal vision, 
        of focusing our energies, of developing patience, and of seeing reality objectively. & 
        \cite{book-senge-1990},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-henriksson-2016},
        \cite{article-org-ghasemi-2017},
        \cite{book-hole-2016},
        \cite{article-markey-2018}
        \\\hline 
        
        Shared mental models & 
        Mental models are deeply ingrained assumptions, generalizations, 
        or even pictures of images that influence how we understand the world and how we take action. & 
        \cite{book-senge-1990},
        \cite{book-hole-2016}
        \\\hline 
        
        Building shared vision & 
        Building shared vision - a practice of unearthing shared pictures 
        of the future that foster genuine commitment and enrollment rather than compliance. & 
        \cite{book-senge-1990},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017}
        \\\hline

        Team learning & 
        Team learning starts with ’dialogue’, the capacity of members of a team to suspend assumptions and enter into genuine ’thinking together’. & 
        \cite{book-senge-1990},
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016},
        \cite{book-hole-2016},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016},
        \cite{thesis-kastner-2017},
        \cite{article-org-ghasemi-2017}
        \\\hline
        
        Systems thinking & 
        Systems thinking is the Fifth Discipline that integrates the other four. Systems thinking also needs the disciplines of building shared vision, mental models, team learning, and personal mastery to realize its potential. & 
        \cite{book-senge-1990},
        \cite{article-johnson-2013-cas},
        \cite{article-se-gorgeon-2015},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017},
        \cite{thesis-kastner-2017},
        \cite{article-markey-2018},
        \cite{article-oreilly-2019}
        \\
          
    \end{tabularx}
 \end{table*}

\subsection{Expert review}

The overall feedback from the one-on-one expert reviews was that the EAAL model sparked inspiration and was relevant to their individual expertise. 
The main feedback that resulted into (re)designing the EAAL model where the following feedback points:
1) All the aspects of a learning organization as described by Senge (1990) are always relevant \cite{book-senge-1990},
2) It is impossible to find out based on observation why something is working. 
Here the person was referring to the function and construction dilemma and to the issue of determining causal relations based on observation.

The overall feedback from the group validations was also that the EAAL model sparked inspiration and is relevant for the future. 
The group of 10 experts also provided feedback that the topic was pretty complex 
and would need examples on how to apply this in their day-to-day work. 

Based on the feedback of the experts, the main adjustment in the EAAL model was that we positioned the learning organization across "attenuate variety and amplify variety". In the first version of the EAAL model, the learning organization was positioned only across "amplify variety". 

\subsection{Practitioner review}

The overall feedback from the practitioners was that the EAAL model sparked inspiration but it can not be used in the boardroom in this form. 
Instead, the EAAL model should be used in
the department that advises the board during the analysis and design process.
Based on the EAAL model, ‘smart‘ questions can be created and asked in the boardroom.

Highlights of the feedback of the interviews are: "For the first time I see a holistic overview of the field of antifragility, resilience and agile.", "This is a powerful tool to think about the future design of the company.", "This all sounds very logical and clear to me. Great to have it in one image.", "After your talk with our CTO he is researching holacracy and the possibilities in the transformation of our organization." But not all the feedback was positive:
"This does not resonate with me. I do not understand what you are trying to tell me."

The feedback of the practitioners did not lead to adjustments of the EAAL model. 