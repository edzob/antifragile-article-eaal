%!TEX root =  ../main.tex
\section{Methodology}

\subsection{Methodology Characteristics}
This research aims to be more exploratory then explanatory. More a field study then a
laboratory setting, with an outcome that is more descriptive then causal with the ambition
to create more understanding then designing 
\cite{book-recker-2013} Emory and Cooper, 1991). 
Since most of the concepts related to antifragile are only described in literature and not yet implemented in practice, a literature review is the basis of the field study. The literature review is used to identify system attributes and plot them on the framework of figure \ref{fig:eaal-categories}. The result is a first draft of the so-called Extended Antifragile Attribute List (EAAL). 

An important remark is that the body of knowledge an antifragile is relatively small and relatively new. As a result, the number of scientific papers is small. That is why we also included non-peer reviewed papers into this research. 

In order to validate the EAAL, reviews are conducted with both experts and practitioners to not only validate the outcome but also to evaluate and improve the outcome \cite{article-newton-2013}. Since the topic of complexity theory and complex adaptive systems is
mostly unknown to the various experts and c-level managers, the naturalistic research is the
most logical research method for the validation of the EAAL model. Naturalistic research is described as: ‘the researcher seeks to make the research experience as
much a part of the subjects’ everyday environment as possible‘ \cite{article-chesebro-2007} 
and as ‘research method to be set in a natural setting in an attempt to explain or interpret a
certain phenomenon‘ \cite{thesis-henriksson-2016, book-alvesson-2009}.

The combination of literature review, expert review, and practitioners review is a form of triangulation. Triangulation is defined as the use of multiple methods mainly qualitative and quantitative
methods in studying the same phenomenon \cite{article-jick-1979, article-hussein-2009}. Triangulation, as in shown in  figure \ref{fig:triangulation}, is applied for the validation of the created framework
\cite{article-hussein-2009, article-jick-1979}.

\begin{figure}[!bth]
    \centering
    \fbox{\includegraphics[width=0.7\linewidth]{eaal-triangulation}}
    \caption{Main research methods \cite{article-alassafi-2017}.}
    \label{fig:triangulation}
\end{figure}  

The combination of research methods applied in this research
can best be described as a
post-positivist 	%\cite{book-denzin-2017-sage, article-ryan-2006}
exploratory 		%\cite{book-recker-2013, book-emory-1991}
qualitative 		%\cite{book-recker-2013, book-emory-1991}
naturalistic  		%\cite{article-chesebro-2007, thesis-henriksson-2016, book-alvesson-2009}
field-study 		%\cite{book-recker-2013, book-emory-1991}
research
\cite{book-denzin-2017-sage, article-ryan-2006,book-recker-2013, book-emory-1991, article-chesebro-2007, thesis-henriksson-2016, book-alvesson-2009}.

In the remainder of this chapter we discuss each of the three parts of figure \ref{fig:triangulation}.

\subsection{Literature Review}

The EAAL Framework is the summary of 
the system attributes found in the literature.
The system attributes that are relevant 
to a resilient and antifragile system
are collected via a critical literature survey.

The collection and summary 
is done via the following six steps.

\begin{enumerate}
	\item Search and select literature (search + snowball).
	
	\item Categorize and summarize the literature.
	
	\item Select most relevant literature.
	
	\item Identify system attributes described in the relevant literature.
	
	\item Create a sorting algorithm (decision tree) to apply to the selected attributes.
	
	\item Sort the identified system attributes by applying the decision tree leading to the EAAL framework.

\end{enumerate}


\subsubsection{Search and select literature}
The search for literature starts 
with the following three types of sources. 
The literature list  
in these sources 
functioned as the start of the snowballs. 

\textit{A. Primary Sources:}
As ‘primary sources‘ we use the references from the book 
(1) Antifragile \cite{book-taleb-2012}, 
and the references from the Wikipedia pages on 
(2) Antifragile and 
(3) Antifragility.

\textit{B. Secondary sources:}
For the ‘secondary sources‘ various academic search engines are used:  
(1) Google Scholar, 
(2) Bing Academic
(3) Semantic Scholar
(4) ReseachGate
(5) Citationsy and the 
(6) Library of Antwerp Management School

\textit{C. Extension on the secondary sources:}
For the ‘extension on the secondary sources‘ 
the following search engines are used: 
(1) Amazon.de, 
(2) Goodreads,
(3) Google Books,
(4) Diva (Sweden), 
(5) Scripties Online (the Netherlands), 
(6) Narcis (the Netherlands), 
(7) OpenThesis.org (USA), and
(8) OATD (Global) 


The following keywords initially 
where used for the search queries for the sources B and C:
`antifragile`;
`anti-fragile`; 
`antifragility`;
`anti-fragility`; 
`Taleb`;
`Nassim Taleb`;
`antifragile organisations`;
`antifragile organizations`;
`anti-fragile organisations`;
`anti-fragile organizations`.
Based on the literature found in the reference lists of the found literature other keywords can be added.

The literature search was conducted between October 2018 and June 2019.

This step resulted in 358 sources\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/wikis/EAAL-literature-thesis}} 
(see Bibliography of \cite{thesis-botjes-2020})
of which in total 87 sources\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/wikis/EAAL-literature-selection}} where categorized
(see Appendix D of \cite{thesis-botjes-2020}). 

The iterative search for papers is also known as 'the snowball approach'  
\cite{article-wohlin-2014, Searchme67:online}.

\subsubsection{Categorization and summarize the literature}
The 87 sources are labeled according to one or more 
of the following six categories :
(1) `Antifragile`; 
(2) `Antifragile \& IT`;
(3) `Organization`; 
(4) `Risk and Resilience`;
(5) `Complexity Science`;
(6) `Science`.


\subsubsection{Select relevant literature}
From the 87 categorized sources nine sources\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/wikis/EAAL-literature-final-selection}} were selected 
based on the following criteria: 
must contain listings of attributes that are linked to antifragile behavior, comprehensiveness, and relevance 
\cite{article-org-ghasemi-2017},
\cite{article-johnson-2013-cas},
\cite{article-org-kennon-2015},
\cite{article-markey-2018},
\cite{thesis-henriksson-2016},
\cite{thesis-kastner-2017},
\cite{article-se-gorgeon-2015},
\cite{book-hole-2016},
\cite{article-oreilly-2019}.

\subsubsection{Select system attributes}
The following attributes where provided in the individual sources. 

Ghasemi and Alizadeh: 
Absorption, 
Redundancy,
Introduction of low level stress Eliminating stress,
Non-monotonicity,
Requisite variety,
Emergence,
Uncoupling 
\cite{article-org-ghasemi-2017}.

Johnson and Gheorghe:
Entropy,
Emergence,
Efficiency vs. Risk,
Balancing Constraints vs. Freedom,
Coupling (Loose/Tight),
Requisite Variety,
Stress Starvation,
Redundancy,
Non-Monotonicity,
Absorption
\cite{article-johnson-2013-cas}.

Kennon et al.:
Emergence,
Efficiency and risk,
Requisite variety,
Stress starvation,
Redundancy,
Absorption,
Induced small stressors,
Non-monotonicity
\cite{article-org-kennon-2015}.

Markey-Towler:
high in Openness,
high in Conscientiousness,
fair degree of Extraversion,
moderate degree of Agreeableness,
low in Neuroticism
\cite{article-markey-2018}.

Henriksson et al.:
Strategy - Design versus emergence,
Strategy - Seneca’s barbell strategy,
Opportunities - Networks,
Opportunities - Innovation,
Opportunities - Resources,
Motivation - Mind-set,
Motivation - Employee motivation,
Motivation - Communication
\cite{thesis-henriksson-2016}.

Kastner:
Self-organization, 
Ownership (result based system /’Skin in the game’),
Diversity of Cells and organizational Learning,
DNA- Shared purpose, values and culture
\cite{thesis-kastner-2017}.

Gorgeon:
Simplicity,
Skin in the Game,
Reduce Naive Interventions,
Optionality,
Inject Randomness into the System,
Decentralize / Develop Layered Systems
\cite{article-se-gorgeon-2015}.

Hole:
Modularity,
Weak Links,
Redundancy,
Diversity,
Fail Fast,
Systemic Failure Without Failed Modules,
The Need for Models
\cite{book-hole-2016}.

O’Reilly:
Modularity,
Weak links,
Redundancy,
Diversity
\cite{article-oreilly-2019}.



\subsubsection{Create sorting algorithm}
The attributes that were selected from the nine sources are sorted according to the decision tree in Figure \ref{fig:model-tree}. The decision tree is based on the framework of Figure \ref{fig:eaal-categories}. The aim of the decision tree is to categorize the attributes. 

\begin{figure}[!tbh]
    \centering
    \fbox{\includegraphics[width=\linewidth]{Figures/eaal-sorting-tree.png}}
    \caption{Decision tree to order the attributes.}
    \label{fig:model-tree}
\end{figure}

\subsubsection{Sort  the  identified  system  attributes}
We followed a creative process in the sorting of the system attributes. The following examples are illustrative for this process. 
Redundancy \cite{book-hole-2016, article-oreilly-2019, article-org-ghasemi-2017, article-org-kennon-2015, article-johnson-2013-cas} does not describe an aspect of the learning organization and its main goal is to maintain the function of a system when a sub-system fails. This attenuates the variety. The use of sub-systems to maintain the functionality is described by Martin-Breen as attribute of Systems Resilience.  
Another example is Efficiency vs. Risk \cite{article-johnson-2013-cas}: this attribute does not fit the learning organization, Efficiency does attenuate variety and is described similar to engineering resilience by Martin-Breen, and Risk is described in line with freedom of employees and fits more Martin-Breen's description of CAS Resilience than Taleb's description of Seneca's barbell. 
The result of this step is the EAAL framework that will be explained in Section \ref{sec:results}.

\subsection{Expert review}
The EAAL framework was validated by 18 experts. Validation took place 
in one-by-one sessions (10 persons) and 
in two group sessions (10 and 3 persons).
There was a small overlap designed in this setup. 

The experts where very divers.	
The expertise topic of the experts are `at least' one of the following list: 
Antifragility, Enterprise
Architecture, Enterprise Engineering, 
Organization design, Organization Change.

The two questions asked to the experts were 
"Does it make sense what I am telling you?", 
"Do you see any big mistakes, blind spots or contrary statements?"

The one-by-one sessions were semi-structured interviews with a duration of
30 minutes to 90 minutes per interview.
The group sessions were in the form of a presentation concluded 
with an open discussion. The duration of these sessions was 120 minutes.



\subsection{Practitioners review}
To review the applicability and relevance of the EAAL framework, seven C-level managers (CFO, CIO, CTO, COO) of seven different organizations where interviewed. Table \ref{tab:eaal-practitionair} contains the demographics of the interviewees. 

The method applied was that of semi-structured interviews with a maximal duration of one hour. 

The questions asked were:
"Do you see or do you hear something that is not right?",
"Do you see or do you hear anything that sounds too far-fetched and only theory
based?", and
"Does what I tell you resonate with you, and can you apply this in your work?".





\begin{table}[ht!]
    \caption{Demographics Practitioners}
    \label{tab:eaal-practitionair}
    \centering
    
    \begin{tabularx}{\columnwidth}{l|X|X}
    \toprule
    Role & Description & FTE \\
    \midrule
    
    CIO & Aviation company & 
    70.000 - 90.000\\
    
    CIO & Aviation company &
    1.000 - 5.000\\

    CIO & Governmental organization & 
    10 - 200\\
    
    COO & Non-profit organization &
    10 - 200\\

    CIO & University &
    200 - 400\\
     
    CFO & University & 
    1.000 - 5.000\\

    CTO & Retail &
    200 - 400 \\

    \end{tabularx}
 \end{table}





