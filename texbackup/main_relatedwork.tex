\section{Framing our research}

In this section, we will clarify how we frame our research. We start by clarifying the motivation for the term \Term{organization} (compared to similar terms such as \Term{enterprise} that are sometimes used). We will also motivate why we consider organizations through the lens of \Term{systems theory}, similar to the work by Morgan \cite{1997-Morgan-Images}, while recognizing that there are different schools of thought in this area (see e.g.\ \cite{2009-Achterbergh-Organizations, 2019-Jackson-SytstemsThinking, 2019-Turner-SystemsComplexity}).

\subsection{Organization} \label{sec:organization}

The first concept that we will consider is \Term{organization}. Myriad definitions exist -- some colloquial, others more formal (e.g. from the realm of organization theory). In our view, there are two key aspects that should be considered:
\begin{itemize}
    \item The first interpretation of the term \Term{organization} is: that which results from the act of organizing. To clarify: suppose we have a collection of Lego bricks. These can be organized in different ways: by randomly tossing them on the table top or, for example, by neatly sorting them by size and color. The fact that our subjective assessment may be that the former process leads to something that is rather `disorganized' is besides the point here. 
    
    \item The second interpretation of the term \Term{organization} refers to organization\emph{s} (plural), recognizing them as a legal entity. In the Dutch language, the category of \Term{Person} (Dutch: \Term{Persoon}) is specialized in two subcategories: \Term{Natural person} (\Term{Natuurlijk persoon}) and \Term{Legal person} (\Term{Rechtspersoon}). 
\end{itemize}
Note that organizations in this second interpretation are the result of the act or organizing, i.e. the first interpretation. In \cite{book-hoogervorst-2017}, the leading term is \Term{enterprise}, which is defined as \textquote{Entities of purposeful human endeavor}. It is also stated that: \blockquote[ibid]{In the case of enterprises, the sensible opposite of doing nothing and the inevitable development of disorder is organizing: the harmonious ordering and arrangement of activities and means in view of the enterprise' purpose(s).} We concur with this view. Yet, making a formal distinction between \Term{enterprise} and \Term{organization} makes this paper needlessly complex. We will only use the term \Term{Organization}. Generally this will refer to the second interpretation. We will explicitly point out where we refer to the first interpretation. 

Stakeholders with regard to the organization have goals. When it is stated that the `organization $x$ has goal $y$' then our interpretation is: stakeholders with regard to organization $x$ agree (to some extent, at least) on the goal $y$. 

\subsection{System} \label{sec:system}

The term \Term{system} is notoriously difficult to define. A full review of the literature on systems theory is beyond the scope of this article. We base our discussion here on \cite{book-ashby-1956, 1998-Falkenberg-FRISCO, 2009-Achterbergh-Organizations, 2019-Jackson-SytstemsThinking, 2019-Turner-SystemsComplexity}. In order to limit the scope of our discussion here further, i.e. to avoid giving a full overview of systems theory, we pose that we are mainly interested in using systems theory as a lens on/ to study organizations -- as defined in the previous section. As such, we are not claiming that organizations \emph{are} a system, merely that considering them through this lens provides useful insights in light of our research goal.

In Ashby's view, a \Term{system} is considered to have a clear boundary, and transforms inputs to outputs \cite{book-ashby-1956}. One of the key results of Ashby's work is the idea that systems are considered as a \Term{black box} and correlate inputs with outputs without understanding the inner construction/ functioning of the system that is studied. This perspective is too limited for our purposes.

In the FRISCO framework \cite{1998-Falkenberg-FRISCO}, a system is defined as \blockquote{A system is a special model, whereby all the things contained in that model (all the system components) are transitively coherent, i.e. all of them are directly or indirectly related to each other from a coherent whole. A system is conceived as having assigned to it, as a whole, a specific characterization (the so-called ``systemic properties'').} whereas a model is defined to be \blockquote{A model is a purposely abstracted, clear, precise, and unambiguous conception.} In other words, a system is considered to be a mental construct and it is at least suggested that a system consists of interconnected \Term{parts}. This fits well with our notion of organizing parts into a whole. 

The authors of the FRISCO framework remark that (ibid) ``The decision where to draw the boundary of the system depends on the system viewer'', which further reinforces the subjective nature of what constitutes a system. We also subscribe to this view. Particularly when systems become more complex, it is harder to precisely determine its boundaries. In this light, it is useful to consider the work by Boulding (See a discussion about \cite{1968-Boulding-GST} in \cite{2019-Jackson-SytstemsThinking}) describing a hierarchy for considering systems on different levels: (1) static structures and frameworks, (2) clockworks, (3) closed-loop control mechanisms, (4) open systems with structural self-maintenance, (5) lower organisms with functional parts and blueprint growth, (6) animals with a brain to guide behavior, capable of learning, (7) people with self-consciousness, (8) socio-cultural systems with roles, communication, and the transmission of values, and (9) transcendental systems, the home of the `inescapable unknowables'. 

From this, we learn that \Term{organizations} have a high level of complexity; they are high-up in the hierarchy. This aligns with the study by Morgan \cite{1997-Morgan-Images} who presents different \emph{images} on \Term{organization} as systems (e.g.\ the organization as a clockwork/ mechanism, as an organism, etc.).

A last point deserves a more careful elaboration. When looking at the hierarchy of Boulding, note that one of the key characteristics of socio-cultural systems is that there is extensive \emph{communication}. From the FRISCO definition for \Term{system}, we know that systems consists of `components'. We pose that (patterns of) communication form the fundamental building blocks of organizations when seen as systems. This aligns with the \emph{language action perspective} and research on DEMO/Enterprise Engineering (e.g.\ \cite{book-dietz-2020, book-hoogervorst-2017}).

\subsection{Complexity}

One aspect that underlies the above mentioned hierarchy of Boulding is the fact that higher levels are more \emph{complex} than lower levels. This begs the question: what is meant by this complexity? In our view, the Cynefin framework by Snowden provides a useful perspective \cite{2003-Kurtz-Cynefin, 2005-Snowden-Cynefin, 2007-Snowden-Cynefin}. This framework is a \Term{sense-making framework}, means that it offers guidance on how to respond in certain situations based on the subjective assessment of the nature of a specific situation by an actor/ decision maker. Cynefin distinguishes the following modes: 
\begin{itemize}
    \item In the \Term{simple domain}, causal effects between variables are apparent, so the proper response is to recognize the situation and act according to the best practice at hand.
    
    \item In the \Term{complicated domain}, causal effects are \emph{knowable}: it may take a lot of time and effort but through analysis they can be found. The proper response is to analyze the situation and then act according to the findings of the analysis.
    
    \item In the \Term{complex domain}, causal effects are too complex for analysis and can only be determined in hindsight. The response mode is: hypothesize what might work, act according to the hypothesis and study the effect of the intervention. If the effect is desirable, the hypothesis was correct. If not, then the effects should be dampened by corrective action.
    
    \item The \Term{chaotic domain} is often seen as the area where immediate action is required in order to return from utter chaos (and potentially threatening situations) to one of the other domains.  
    
    \item The \Term{disorder domain} is the `catchall' domain, representing situations where an actor/decision maker has not yet come to a conclusion in which of the `other' domains s/he is.
\end{itemize}
Here, we are not making any claims about the level of complexity of organizations - even though the Boulding hierarchy suggests that \Term{organizations} as defined in this article are likely to be in the complex domain. In our view, the level of complexity depends on how much is known about a given domain and the perspective may shift over time: what was considered to be \emph{complex} in the early days of the industrial age, is likely to be \emph{complicated} or perhaps even \emph{simple} with our current understanding of organizations and society. In light of our objective to understand the antifragility of organizations, this means that we have to take into account how much stakeholders (can) know about the organizations that we are studying. 

\subsection{Categories of system attributes}

We consider organizations as complex adaptive systems (ref needed?). In order to identify what kind of attributes make an organization antifragile we need to include different system attributes and categorize these attributes. This section describes the categories of system attributes we found in literature. The main concepts we discuss are (anti)fragile, resilience, variety, and the learning organization. 

Antifragile can be described as a way to deal with stressors. The term stressor is used for an event from outside the system that causes stress. For this study we use the following definition of a stressor: “when systems are performing effectively, they are in a predetermined condition and conversely when they are not functioning correctly, they are in an unintended state. An unintended condition can be known or unknown. Stressors are forces that threaten to transfer a system from an intended to an unintended condition” \cite{article-turner-2003, article-chrousos-2009}. Fragile is the concept of losing value from exposure to stressors. Antifragile is the antithesis of fragile. Antifragile is the concept of gaining value from exposure to stressors. 
The concept of stressors having no effect on the value is called robust \cite{book-taleb-2012}. 
Black swans (or X-events) are defined as stressor events in the VUCA domain that have a disruptive impact on a fragile and robust system and are non-predictable 
\cite{book-taleb-2012, book-casti-2012, book-hole-2016}. 
Fragile, robust and antifragile form together a triad 
\cite{book-taleb-2012, article-se-gorgeon-2015, article-org-kennon-2015, book-hole-2016, article-org-ghasemi-2017}. 

Figure \ref{fig:eaal-triad} illustrates the differences in the way the three main categories deal with stress.

\begin{figure}[tbh!]
    \centering
    \fbox{\includegraphics[width=\linewidth]{eaal-triad}}
    \caption{Triad of fragile, robust and antifragile}
    \label{fig:eaal-triad}
\end{figure}


Resilience is a concept that is often mentioned related to (anti)fragile. Resilience is the ability to recover from or adjust easily to misfortune or change \cite{article-martin-breen-2011}. 

Figure \ref{fig:eaal-resilience} illustrates the generic concept of resilience.

\begin{figure}[bth!]
    \centering
    \fbox{\includegraphics[width=\linewidth]{eaal-resilience-grey-border}}
    \caption{Generic concept of resilience}
    \label{fig:eaal-resilience}
\end{figure}

Resilience falls into three subtypes: 
1) engineering resilience, 
2) systems resilience, and 
3) complex adaptive systems (CAS) resilience, as identified by \cite{article-martin-breen-2011, thesis-kastner-2017}. The goal of engineering resilience is to prevent disruption and changes. The goal is to bounce back to the fixed function/basis \cite{article-holling-1996, article-martin-breen-2011, thesis-kastner-2017}. Engineering resilience can be measured by the following three characteristics: resistance, elasticity and stability \cite{article-martin-breen-2011}. The function and construction of the system stay the same over time. In case of systems resilience, the system has the capacity to absorb disturbance and reorganize while undergoing changes. While doing this, the systems retains essentially the same function, structure, identity, and feedback, where ’essential’ is defined as ‘something functional and not identical’ \cite{article-walker-2004, article-martin-breen-2011}. The system is able to withstand the impact of any interruption and recuperate while resuming its operations \cite{article-santos-2012}. The function of the system stays the same over time. The construction of the system may change. With CAS resilience, the system is able to become more resilient and to generate new system relationships by reorganization \cite{article-martin-breen-2011, thesis-kastner-2017}. The function is maintained, but system structure may change \cite{article-martin-breen-2011}. This results in the system to being as dynamic as the world around them, thus a system that is constantly evolving \cite{thesis-kastner-2017}. The function of the system may change over time, and the construction of the system may change over time. 

Figure \ref{fig:eaal-martin-breen} illustrates the differences in the way the three subtypes of resilience deal with stress.

\begin{figure}[tbh!]
    \centering
    \fbox{\includegraphics[width=\linewidth]{eaal-martin-breen}}
    \caption{Three subtypes of resilience by Martin-Breen 2011}
    \label{fig:eaal-martin-breen}
\end{figure}

A different way to study complex adaptive systems is the concept of variety. Two types of variety are based Ashby's and Beer's work on cybernetics: 1) attenuate variety and 2) amplify variety \cite{book-ashby-1956, book-beer-1979}. Attenuate variety is the concept of reducing the variety in a system. The absorption of change in the context of systems is called attenuate variety or variety reduction. Amplify variety is the concept of increasing the variety in a system. Amplify variety is about increasing the chance of a higher entropy and therefore being more capable to absorb increasing variety caused by change. Emergence leads to variety amplification. Engineering and systems resilience are considered a form of attenuate variety. CAS resilience and antifragile are considered a form of amplify variety.

A final way to deal with stressors is to adapt concepts from the learning organization as described by Senge \cite{book-senge-1990}. The learning organization is a way to create resilient organizations which let them cope with unknown and unpredictable events. Senge identifies five disciplines that together form the learning organization: 1) Personal mastery, which is a discipline of continually clarifying and deepening our personal vision, of focusing our energies, of developing patience, and of seeing reality objectively; 2) Shared mental models. Mental models are deeply ingrained assumptions, generalizations, or even pictures of images that influence how we understand the world and how we take action; 3) Building shared vision, which is a practice of unearthing shared pictures of the future that foster genuine commitment and enrollment rather than compliance; 4) Team learning, that starts with ’dialogue’, the capacity of members of a team to suspend assumptions and enter into genuine ’thinking together’, and finally 5) Systems thinking. This is the fifth discipline that integrates the other four. Systems thinking needs the disciplines of building shared vision, mental models, team learning, and personal mastery to realize its potential. Building shared vision fosters a commitment to the long term. Mental models focus on the openness needed to unearth shortcomings in our present ways of seeing the world. Team learning develops the skills of groups of people to look for the larger picture beyond individual perspectives. And personal mastery fosters the personal motivation to continually learn how our actions affect our world \cite{book-senge-1990}.

Figure \ref{fig:eaal-categories} comprises the framework that we will use to categorize system attributes. 

\begin{figure}[tbh!]
    \centering
    \fbox{\includegraphics[width=\linewidth]{EAAL-categories}}
    \caption{Framework to categorize system attributes}
    \label{fig:eaal-categories}
\end{figure}  
