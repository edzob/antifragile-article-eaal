\section{Results} \label{sec:results}


\subsection{Step 1: search and select literature} 
The search for literature starts with the following three types of sources. 
The literature list in these sources functioned as the start of the snowballs \cite{article-wohlin-2014, Searchme67:online}. 
As primary sources we use the references from the book Antifragile by Taleb \cite{book-taleb-2012} and the references from the Wikipedia pages on `Antifragile' and `Antifragility'. 
For the secondary sources, several academic search engines are used, namely: (1) Google Scholar, (2) Bing Academic, (3) Semantic Scholar, (4) ReseachGate, (5) Citationsy, and the (6) Library of Antwerp Management School.
These are further extended by additional searches in: (7) Amazon.de, (8) Goodreads, (9) Google Books, (10) Diva (Sweden), (11) Scripties Online (the Netherlands), (12) Narcis (the Netherlands), 
(13) OpenThesis.org (USA), and (14) OATD (Global). 

The following keywords initially where used for the search queries: \Term{antifragile}, \Term{anti-fragile}, \Term{antifragility}, \Term{anti-fragility}, \Term{Taleb}, \Term{Nassim Taleb}, \Term{antifragile organisations}, \Term{antifragile organizations}, \Term{anti-fragile organisations}, \Term{anti-fragile organizations}.
The literature search was conducted between October 2018 and June 2019.
This step resulted in 358 sources\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/wikis/EAAL-literature-thesis}}, of which in total 87 sources\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/wikis/EAAL-literature-selection}} where categorized in the following research steps. 

\subsection{Step 2: categorization and summarize the literature}

The 87 sources are labeled according to one or more of the following six categories: (1) \Term{Antifragile}, (2) \Term{Antifragile \& IT}, (3) \Term{Organization}, (4) \Term{Risk and Resilience}, (5) \Term{Complexity Science}, and (6) \Term{Science}.
These categories emerged during the creative process of labeling and were validated with experts and practitioners in the triangulation as described in Section~\ref{sec:meth-triangulation}.


\subsection{Step 3: select relevant literature}

A first reading of the 87 categorized sources suggested that several sources were not as relevant as initially hoped and expected. 
To narrow down the set, we used a filter with the following criteria: \emph{must contain listings of attributes that are linked to antifragile behavior, comprehensiveness, and relevance }. This narrowed the list of sources to nine: \cite{article-org-ghasemi-2017}, \cite{article-johnson-2013-cas}, \cite{article-org-kennon-2015}, \cite{article-markey-2018}, \cite{thesis-henriksson-2016}, \cite{thesis-kastner-2017}, \cite{article-se-gorgeon-2015}, \cite{book-hole-2016}, \cite{article-oreilly-2019}.

\subsection{Step 4: select system attributes}

The following attributes where provided in the individual sources. 
\begin{itemize}
    \item Ghasemi and Alizadeh \cite{article-org-ghasemi-2017}: Absorption, Redundancy, Introduction of low level stress, Eliminating stress, Non-monotonicity, Requisite variety, Emergence, Uncoupling.

    \item Johnson and Gheorghe \cite{article-johnson-2013-cas}: Entropy, Emergence, Efficiency vs. risk, Balancing constraints vs. freedom, Coupling (loose/tight), Requisite variety, Stress starvation, Redundancy, Non-monotonicity, Absorption.

    \item Kennon et al. \cite{article-org-kennon-2015}: Emergence, Efficiency and risk, Requisite variety, Stress starvation, Redundancy, Absorption, Induced small stressors, Non-monotonicity.

    \item Markey-Towler \cite{article-markey-2018}: High in openness, High in conscientiousness, Fair degree of extraversion, Moderate degree of agreeableness, Low in neuroticism.

    \item Henriksson et al. \cite{thesis-henriksson-2016}: Strategy - Design versus emergence, Strategy - Seneca’s barbell strategy, Opportunities - networks, Opportunities - innovation, Opportunities - resources, Motivation - mind-set, Motivation - employee motivation, Motivation - communication

    \item Kastner \cite{thesis-kastner-2017}: Self-organization,  Ownership (result based system /’skin in the game’), Diversity of cells and organizational learning, DNA-shared purpose, values and culture.
    
    \item Gorgeon \cite{article-se-gorgeon-2015}: Simplicity, Skin in the game, Reduce naive interventions, Optionality, Inject randomness into the system, Decentralize / develop layered systems.

    \item Hole \cite{book-hole-2016}: Modularity, Weak links, Redundancy, Diversity, Fail fast, Systemic failure without failed modules, The need for models.

    \item O'Reilly \cite{article-oreilly-2019}: Modularity, Weak links, Redundancy, Diversity.
\end{itemize}


\subsection{Step 5: create sorting algorithm for organizations}

In order to transform the list of attributes into a framework, we needed to develop a sorting algorithm that helps us to determine which categories we should use, and which attributes belong in a specific category.
The sorting algorithm was developed in several iterations, and is based on a second study of the available literature. 
As before, we used triangulation to validate the sorting algorithm as discussed in Section~\ref{sec:meth-triangulation}. 


Figure \ref{fig:eaal-categories} comprises the framework that we we designed to categorize system attributes, which is mainly based on the discussion of (anti)fragile, variety, and the learning organization in Section~\ref{sec:antifraglearning}.
\begin{figure}[htb]
    \centering
    \fbox{\includegraphics[width=\linewidth]{EAAL-categories}}
    \caption{Framework to categorize system attributes}
    \label{fig:eaal-categories}
\end{figure}  

Using this framework, we ended up with the decision tree that is visualized in Figure \ref{fig:model-tree}. 
\begin{figure}[htb]
    \centering
    \fbox{\includegraphics[width=\linewidth]{Figures/eaal-sorting-tree.png}}
    \caption{Decision tree to order the attributes.}
    \label{fig:model-tree}
\end{figure}

\subsection{Step 6: sort  the  identified  system  attributes}

While we initially hoped that classifying the attributes with our decision tree would be an almost mechanical process, this turned out to be not feasible. 
The overall context of the description of the attribute in the selected works and expert judgment were needed to come to a sort. 
Therefore, we ended up following a creative process in the sorting of the attributes. 
The following examples are illustrative for this process:
\begin{itemize}
    \item Redundancy was identified in \cite{book-hole-2016, article-oreilly-2019, article-org-ghasemi-2017, article-org-kennon-2015, article-johnson-2013-cas}. It does not describe an aspect of the learning organization and its main goal is to maintain the function of a system when a sub-system fails. This attenuates the variety. The use of sub-systems to maintain the functionality is described by Martin-Breen \cite{article-martin-breen-2011} as attribute of Systems Resilience. 
    
    \item Another example is Efficiency vs. risk as discussed in \cite{article-johnson-2013-cas}. This attribute does not fit the learning organization. Efficiency does attenuate variety and is described similar to engineering resilience by Martin-Breen  \cite{article-martin-breen-2011}, and risk is described in line with freedom of employees and fits more Martin-Breen's description of CAS Resilience than Taleb's description of Seneca's barbell. 
\end{itemize}
The result of this step is the EAAL framework that will be explained later in this Section. 

\subsection{Step 7: reviews}

\subsubsection{Expert review}
The EAAL framework was validated by 18 experts. 
Validation took place in one-by-one sessions (10 persons) and 
in two group sessions (10 and 3 persons).
There was a small overlap designed in this setup. 

The experts where very diverse.	
The expertise topic of the experts are `at least' one of the following list: antifragility, enterprise architecture, enterprise engineering, organization design, organization change.

The two questions asked to the experts were: (1) Does it make sense what I am telling you?, and (2) Do you see any big mistakes, blind spots or contrary statements?.

The one-by-one sessions were semi-structured interviews with a duration of 30 minutes to 90 minutes per interview.
The group sessions were in the form of a presentation concluded with an open discussion. 
The duration of these sessions was 120 minutes.

\subsubsection{Practitioners review}
To review the applicability and relevance of the EAAL framework, seven C-level managers (CFO, CIO, CTO, COO) of seven different organizations where interviewed. 
Table \ref{tab:eaal-practitionair} contains the demographics of the interviewees. 
The method applied was that of semi-structured interviews with a maximum duration of one hour. 

The questions asked were: (1) Do you see or do you hear something that is not right?, (2) Do you see or do you hear anything that sounds too far-fetched and only theory based?, and (3) Does what I tell you resonate with you, and can you apply this in your work?

\begin{table}[ht!]
    \caption{Demographics Practitioners}
    \label{tab:eaal-practitionair}
    \centering
    
    \begin{tabularx}{\columnwidth}{l|X|X}
    \toprule
    Role & Type of organization & FTE \\
    \midrule
    
    CIO & Aviation company & 
    70.000 - 90.000\\
    
    CIO & Aviation company &
    1.000 - 5.000\\

    CIO & Governmental organization & 
    10 - 200\\
    
    COO & Non-profit organization &
    10 - 200\\

    CIO & University &
    200 - 400\\
     
    CFO & University & 
    1.000 - 5.000\\

    CTO & Retail &
    200 - 400 \\

    \end{tabularx}
 \end{table}

\subsection{Step 8: EAAL framework}

The EAAL framework is the result of synthesizing all previous results as discussed in this article. 
In essense, this meant that we had to plot the attributes that resulted from step~\ref{stepfw} back on the main framework that was presented in Figure~\ref{fig:eaal-categories}.
Figure \ref{fig:eaal-framework} shows the result of this analysis, and contains the EAAL framework. Table \ref{tab:eaal-framework} defines the different attributes of the EAAL framework. 

\begin{figure}[htb]
    \centering
    \fbox{\includegraphics[width=\linewidth]{eaal-framework}}
    \caption{EAAL Framework}
    \label{fig:eaal-framework}
\end{figure}

\begin{table*}[htb]
    \caption{EAAL Attributes}
    \label{tab:eaal-framework}
    \centering
    
    \begin{tabularx}{\textwidth}{l|X|R{2.5cm}}

        \textbf{Attribute} &
        \textbf{Description} &
        \textbf{References}
        \\
        \hline 
        
        %\multicolumn{3}{l}{\textbf{Engineering Resilience} }  \\\hline
        
        Top-down C\&C & 
        Top-down command and control applies when  
        an employee does not have the freedom 
        to decide their own action
        but has to follow instructions 
        from the organizational hierarchy. 

        The careful design of the features of an iPhone or a good pen 
        are examples of limited freedom of movement in the product itself. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas}, 
        \cite{thesis-henriksson-2016}, 
        \cite{thesis-kastner-2017}, 
        \cite{article-oreilly-2019}
        \\\hline

        Micro-management  & 
        Micro-management entails the freedom in the use of the product. 
        A detailed working instruction describing a business process, 
        results in no freedom for the employee in the execution of their job. 
        An example is a Lego building block. 
        It is engineered and fabricated 
        with the greatest detail resulting in a building block 
        that is almost completely robust. 
        Lego has a very small resilience behavior through engineering. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas}, 
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016}, 
        \cite{article-org-ghasemi-2017}, 
        \cite{thesis-kastner-2017}
        \\
        \hline

        %\addlinespace
        %\multicolumn{3}{l}{\textbf{Systems Resilience} } \\\hline
        
        Redundancy & 
        Redundancy is about having not a single point of failure 
        by making use of duplication. 
        
        An example is a backup electricity generator. 
        Another example is local government 
        as backup system of the central government. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}, 
        \cite{article-org-kennon-2015}, 
        \cite{article-oreilly-2019}
        \\\hline       
        
        Modularity & 
        Modularity is the degree that components may be separated and recombined, 
        often with the benefit of flexibility. 
        For example, a car with a standard chassis onto which different components can be connected creating a unique car. & 
        \cite{article-martin-breen-2011}, 
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016}, 
        \cite{article-oreilly-2019}
        \\\hline        
        
        Loosely coupled & 
        Loosely coupled is the degree of dependency on the exact working of another module. It is important to understand that there is always some degree of coupling. Loosely coupled is also known as ‘weak links‘, ‘uncoupling‘, 
        ‘loose/tight coupling‘, or ‘a low level of interconnectedness between components‘. For example, 
        when there are new employees introduced at the finance department 
        this should not impact the taste of the coffee at the same office. &
        \cite{article-martin-breen-2011},
        \cite{article-johnson-2013-cas},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016}, 
        \cite{article-org-ghasemi-2017},
        \cite{article-oreilly-2019},
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{\textbf{CAS Resilience} } \\\hline
      
        Diversity &  
        Diversity is the ability to solve a problem in more than one way with different components. 
        Optionality, the availability of options, is a specialisation of diversity.
        An example is that within a team you want diverse co-workers 
        since other types of people come up with other types of solutions.
        & 
        \cite{article-martin-breen-2011}, 
        \cite{book-taleb-2012},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017},
        \cite{article-oreilly-2019}
        \\\hline    
        
        Non-monotonicity & 
        Non-monotonicity is learning from bad experiences. 
        Mistakes and failures can lead to new information. 
        As new information becomes available it defeats previous thinking, 
        which can result in new practices and approaches. & 
        \cite{article-johnson-2013-cas},  
        \cite{article-se-gorgeon-2015},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}
        \\\hline    
        
        Emergence & 
        When there is little or no traceable relation between
        micro and macro level output then emergence is there.
        This is the situation where random things (unintended states) appear 
        more often and X-events (black swans) appear. 
        The law or requisite variety applied in this reasoning, 
        makes that internal emergence counters external emergence, 
        and this subsequently leads to antifragility & 
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},  
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017},
        \cite{article-org-ghasemi-2017}
        \\\hline        
        
        Self-organization & 
        Self-organization is a process where some form 
        of overall order arises from local interactions 
        between parts of an initially disordered system. For example, 
        students sitting together in the school cafeteria. & 
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017}
        \\\hline    
        
        Insert low-level stress & 
        Continuous improvement is achieved 
        by inserting low-level of stress continuously into a learning system. 
        This will keep the system sharp all the time. & 
        \cite{book-taleb-2012},
        \cite{article-org-kennon-2015},  
        \cite{article-se-gorgeon-2015},
        \cite{article-org-ghasemi-2017} 
        \\\hline    
        
        Network-connections & 
        A network is created by connections to other nodes. 
        More connections increase the potential for optionality 
        for new constructions and also new functionalities. & 
        \cite{article-johnson-2013-cas}, 
        \cite{article-se-gorgeon-2015},
        \cite{thesis-henriksson-2016},
        \cite{book-hole-2016},
        \cite{thesis-kastner-2017}, 
        \cite{article-org-ghasemi-2017}, 
        \cite{article-markey-2018},
        \cite{article-oreilly-2019} 
        \\\hline 

        Fail fast & 
        The other combined attributes in this group enable the possibility 
        to execute the strategy “fail fast”. & 
        \cite{article-org-kennon-2015},   
        \cite{article-se-gorgeon-2015},    
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017}
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{\textbf{Antifragile} } \\\hline
       
        Resources to invest & 
        Opportunities can only be seized when there are resources free to do so. 
        Resources can be money but also time and labor. 
        To survive, a black swan investment should be possible when required & 
        \cite{book-taleb-2012},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017},
        \cite{thesis-henriksson-2016} 
        \\\hline  

        Seneca’s barbell &  
        To be antifragile a robust sub-system 
        is needed to which 80-90\% predictable value with low risk is situated.
        The remaining 10-20\% should be used for high return 
        on investment activities.  & 
        \cite{book-taleb-2012},
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016}
        \\\hline 
        
        Insert randomness & 
        When insert-low-level stress and fail fast delivers no issues 
        the next step is to insert randomness into the systems. 
        A great example of this is chaos engineering by Netflix 
        or the HackerOne bug-bounty system. & 
        \cite{book-taleb-2012},
        \cite{article-org-kennon-2015},
        \cite{article-se-gorgeon-2015},
        \cite{article-org-ghasemi-2017}
        \\\hline 
        
        Reduce naive intervention & 
        Naive intervention is an intervention based on a model 
        and reductionistic logic which ignores experience. 
        An example is not listening to the experienced 
        but not so articulate employee, 
        or by ignoring the balance nature has found in an ecosystem. & 
        \cite{book-taleb-2012}, 
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017}
        \\\hline

        Skin in the game &  
        Make certain that the person making the decision 
        and doing the work has a pain and gain relation with the outcome. 
        This goes beyond having a feedback system in place. 
        This goes beyond having KPI’s in place. 
        An example is that when working Agile scrum, 
        the product owner should be a co-worker in the team 
        for whom the solution is build. & 
        \cite{book-taleb-2012}, 
        \cite{thesis-henriksson-2016},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-kastner-2017}
        \\
        \hline
        
        %\addlinespace
        %\multicolumn{3}{l}{ \textbf{Learning Organization} }\\\hline
        
        Personal mastery & 
        Personal mastery is a discipline of continually clarifying and deepening our personal vision, 
        of focusing our energies, of developing patience, and of seeing reality objectively. & 
        \cite{book-senge-1990},
        \cite{article-se-gorgeon-2015},
        \cite{thesis-henriksson-2016},
        \cite{article-org-ghasemi-2017},
        \cite{book-hole-2016},
        \cite{article-markey-2018}
        \\\hline 
        
        Shared mental models & 
        Mental models are deeply ingrained assumptions, generalizations, 
        or even pictures of images that influence how we understand the world and how we take action. & 
        \cite{book-senge-1990},
        \cite{book-hole-2016}
        \\\hline 
        
        Building shared vision & 
        Building shared vision - a practice of unearthing shared pictures 
        of the future that foster genuine commitment and enrollment rather than compliance. & 
        \cite{book-senge-1990},
        \cite{thesis-henriksson-2016},
        \cite{thesis-kastner-2017}
        \\\hline

        Team learning & 
        Team learning starts with ’dialogue’, the capacity of members of a team to suspend assumptions and enter into genuine ’thinking together’. & 
        \cite{book-senge-1990},
        \cite{article-johnson-2013-cas},
        \cite{article-org-kennon-2015},
        \cite{thesis-henriksson-2016},
        \cite{book-hole-2016},
        \cite{article-se-gorgeon-2015},
        \cite{book-hole-2016},
        \cite{thesis-kastner-2017},
        \cite{article-org-ghasemi-2017}
        \\\hline
        
        Systems thinking & 
        Systems thinking is the Fifth Discipline that integrates the other four. Systems thinking also needs the disciplines of building shared vision, mental models, team learning, and personal mastery to realize its potential. & 
        \cite{book-senge-1990},
        \cite{article-johnson-2013-cas},
        \cite{article-se-gorgeon-2015},
        \cite{article-org-kennon-2015},
        \cite{book-hole-2016},
        \cite{article-org-ghasemi-2017},
        \cite{thesis-kastner-2017},
        \cite{article-markey-2018},
        \cite{article-oreilly-2019}
        \\
          
    \end{tabularx}
\end{table*}

\subsection{Step 9: reviews}

\subsubsection{Expert review}

The overall feedback from the one-on-one expert reviews was that the EAAL model sparked inspiration and was relevant to their individual expertise. 
The main feedback that resulted into (re)designing the EAAL model where the following feedback points:
(1) All the aspects of a learning organization as described by Senge (1990) are always relevant \cite{book-senge-1990},
(2) It is impossible to find out based on observation why something is working. 
In this case, the expert was referring to the function and construction dilemma and to the issue of determining causal relations based on observation.

The overall feedback from the group validations was also that the EAAL model sparked inspiration and is relevant for the future. 
The group of 10 experts also provided feedback that the topic was pretty complex and would need examples on how to apply this in their day-to-day work. 

Based on the feedback of the experts, the main adjustment in the EAAL model was that we positioned the learning organization across ``attenuate variety and amplify variety''. 
In the first version of the EAAL model, the learning organization was positioned only across "amplify variety". 

\subsubsection{Practitioner review}

The overall feedback from the practitioners was that the EAAL model sparked inspiration but it can not be used in the boardroom in this form. 
Instead, the EAAL model should be used in
the department that advises the board during the analysis and design process.
Based on the EAAL model, `smart' questions can be created and asked in the boardroom.

Highlights of the feedback of the interviews are: 
\begin{itemize}
    \item ``For the first time I see a holistic overview of the field of antifragility, resilience and agile''.
    
    \item ``This is a powerful tool to think about the future design of the company''.
    
    \item ``This does not resonate with me. I do not understand what you are trying to tell me''.
    
    \item ``This all sounds very logical and clear to me. Great to have it in one image''.
    
    \item ``After your talk with our CTO he is researching holacracy and the possibilities in the transformation of our organization''.
    

\end{itemize}
The feedback of the practitioners did not lead to adjustments of the EAAL model. 