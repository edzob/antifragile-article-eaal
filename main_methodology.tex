\section{Methodology} \label{sec:methodology}

In order to answer our research question, we need two things: first, we need to understand which characteristics influence the antifragility of organizations, when studied through the lens of organizations as complex systems. 
Then, we need to arrange these characteristics in a structured framework.

The key input for these two steps is a survey of the available literature that is validated by experts and professionals. This leads to a research approach with the following steps:
\begin{enumerate}
	\item Search and select literature (search + snowball).
	
	\item Categorize and summarize the literature.
	
	\item Select most relevant literature.
	
	\item Identify system attributes described in the relevant literature.
	
	\item Create a framework to categorize system attributes and use this to design a sorting algorithm (decision tree) to apply to the selected attributes. \label{stepfw}
	
	\item Sort the identified system attributes by applying the decision tree.
	
	\item Validate the results so far with experts and professionals. 
	
	\item Design the final framework by mapping the sorted attributes back on the framework that was designed in step~\ref{stepfw}.

    \item Validate the final framework with experts and professionals.
\end{enumerate}
Section~\ref{sec:meth-lit} describes the characteristics of our literature survey in more detail. 
Section~\ref{sec:meth-triangulation} explains how we used triangulation to safeguard the quality of our results. 
The combination of research methods applied in this research
can best be described as a
post-positivist 	%\cite{book-denzin-2017-sage, article-ryan-2006}
exploratory 		%\cite{book-recker-2013, book-emory-1991}
qualitative 		%\cite{book-recker-2013, book-emory-1991}
naturalistic  		%\cite{article-chesebro-2007, thesis-henriksson-2016, book-alvesson-2009}
field-study 		%\cite{book-recker-2013, book-emory-1991}
research
\cite{book-denzin-2017-sage, article-ryan-2006,book-recker-2013, book-emory-1991, article-chesebro-2007, thesis-henriksson-2016, book-alvesson-2009}.


\subsection{Characteristics of validated literature survey} \label{sec:meth-lit}

The literature review is used to identify system attributes, and also to design the framework in which these attributes are ordered. % and plot them on the framework of figure \ref{fig:eaal-categories}. 

An initial scan of the available literature on antifragility suggests that the body of \emph{scientific} literature (i.e. peer-reviewed) is relatively small, and that there are few case reports that describe how lessons learned have been applied in practice.
Similarly, the `practical' body of knowledge on antifragility is relatively small which is unsurprising for such a novel topic.
This impacts our methodology in the sense that a critical literature survey will likely yield few results. 
For other (related) topics (e.g. systems theory, complexity theory), the body of research is more mature. 
To counter the risks of an unbalanced critical literature survey, we have adopted an approach that is exploratory in nature: we focus on the \emph{knowledge question} of which attributes impact the antifragility of the organization, rather than the \emph{design question} of how such attributes can be impacted through interventions \cite{book-recker-2013, wieringa2014design}. 

In order to validate the results of the literature, reviews are conducted with both experts and practitioners.
The objective of this validation is to evaluate and improve the outcome of the literature survey \cite{article-newton-2013}. 
Since the topic of complexity theory and complex adaptive systems is
mostly unknown to the various experts and c-level managers, the naturalistic research approach is the most logical research method for the validation of the EAAL model. 
Naturalistic research is described as: \enquote{The researcher seeks to make the research experience as much a part of the subjects' everyday environment as possible} \cite{article-chesebro-2007}, and as \enquote{A research method to be set in a natural setting in an attempt to explain or interpret a certain phenomenon} \cite{thesis-henriksson-2016, book-alvesson-2009}.

\subsection{Triangulation} \label{sec:meth-triangulation}
The combination of literature review, expert review, and practitioners review is a form of triangulation. 
Triangulation is defined as the use of multiple methods mainly qualitative and quantitative
methods in studying the same phenomenon \cite{article-jick-1979, article-hussein-2009}. 
Triangulation, as in shown in  figure \ref{fig:triangulation}, is applied for the validation of the created framework, consisting of the list of attributes found in the literature survey
\cite{article-hussein-2009, article-jick-1979}.

\begin{figure}[!bth]
    \centering
    \fbox{\includegraphics[width=0.7\linewidth]{eaal-triangulation}}
    \caption{Main research methods \cite{article-alassafi-2017}.}
    \label{fig:triangulation}
\end{figure}  






